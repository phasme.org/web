from scss.compiler import Compiler
from scss.namespace import Namespace
from phasme.config import Config

### Command entry point
def run(source, file, target, *args, **kvargs)->None:
    """
    """

    ### Variables
    source = source / file
    target = target / file
    config = Config().get(__name__)
    options = config['pyscss']

    ### Get scss compiler
    # Warning when using Python 3.11 - https://github.com/Kronuz/pyScss/pull/426
    namespace = Namespace()
    compiler = Compiler(
        namespace=namespace,
        **options
    )

    ###
    ### Compile file
    ###
    with open(target, mode='wt') as handler:

        ### Source is a regular file.
        if source.is_file():
            handler.write(compiler.compile(str( source )))

        ### Source is a directory:
        if source.is_dir():
            index = config['index']
            handler.write(compiler.compile(str( source / index )))