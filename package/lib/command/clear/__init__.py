from phasme.utils import call
def run(*args,**kvargs):
    call('clear.target', *args, **kvargs)
    call('clear.temp', *args, **kvargs)