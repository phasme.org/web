from phasme.utils import call

###
### Main
###
def run(*args)->None:
    """
    This is a virtual command for:

    - build.theme
    - build.resources
    - build.project
    """
    call('build.theme', *args)
    call('build.resources', *args)
    call('build.project', *args)
