from jinja2 import nodes
from jinja2.ext import Extension
from jinja2.nodes import CallBlock
from jinja2.exceptions import TemplateSyntaxError

class StackExtension(Extension):
    
    tags = set(['stack','push'])
    
    __stack = {}

    def __init__(self, environment) -> None:
        super(StackExtension, self).__init__(environment)
        environment.globals.update(has_stack=self.has_stack)

    def parse(self, parser):
        _tag = parser.stream.current.value

        ### Go to next token
        next(parser.stream)

        ### This is a stack
        if _tag == 'stack':
            return self.__stack_parse(parser=parser)
        
        ### This is a push
        if _tag == 'push':
            return self.__push_parse(parser=parser)

        ### Should never reach this point
        raise Exception("Should never reach this point")
    
    ###
    ### Public functions
    ###

    def has_stack(self, name:str) -> bool:
        """
        """
        return name in self.__stack and self.__stack[name]
    
    ###
    ### Internal
    ###

    def __get_or_create_stack(self, name:str) -> list:
        """
        """
        if name not in self.__stack:
            self.__stack[name] = []
        return self.__stack[name]

    ###
    ### Push
    ###

    def __push_parse(self, parser):
        lineno = parser.stream.current.lineno
        
        ### Extract stack name
        if parser.stream.current.type == 'name':
            args = [nodes.Const(parser.parse_expression().name)]
        elif parser.stream.current.type == 'string':
            args = [parser.parse_expression()]
        else:
            raise TemplateSyntaxError('Meaningless argument for block "stack"', lineno, parser.name, parser.filename )
        
        ### If there is a comma, the user provided a priority.
        if parser.stream.skip_if('comma'):
            if parser.stream.current.type == 'integer':
                args.append(parser.parse_expression())
            else:
                raise TemplateSyntaxError('Meaningless argument for block "push"', lineno, parser.name, parser.filename )
        else:
            args.append(nodes.Const(None))

        ### Get tag content
        body = parser.parse_statements(("name:endpush", ), drop_needle=True)

        print('parse:push', args)

        ### Completed
        return CallBlock(
            self.call_method("_push_render", args),
            [],
            [],
            body
        ).set_lineno(lineno)

    def _push_render(self, name, priority, caller) -> str:
        stack = self.__get_or_create_stack(name)

        ### This is a priority push
        if isinstance(priority, int):
            stack.insert(priority, caller())

        ### This is a standard push
        else:
            stack.append(caller())

        print('render:push', name, priority)

        ### Completed
        return ''

    ###
    ### Stack
    ###

    def __stack_parse(self, parser):
        lineno = parser.stream.current.lineno

        ### Extract stack name
        if parser.stream.current.type == 'name':
            args = [nodes.Const(parser.parse_expression().name)]
        elif parser.stream.current.type == 'string':
            args = [parser.parse_expression()]
        else:
            raise TemplateSyntaxError('Meaningless argument for block "stack"', lineno, parser.name, parser.filename )

        ### Get tag content
        body = parser.parse_statements(
            ("name:endstack", ),
            drop_needle=True
        )

        ### Completed
        return CallBlock(
            self.call_method("_stack_render", args),
            [],
            [],
            body
        ).set_lineno(lineno)

    def _stack_render(self, name, caller) -> str:
        stack = self.__get_or_create_stack(name)

        ### Stack do contains data
        if stack:
            return ''.join(stack)

        ### Stack is empty
        return caller()