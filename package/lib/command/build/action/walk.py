from phasme.utils import call
import logging
import re

### Global variables
_logger = logging.getLogger(__name__)

###
### Main
###

def run(source, target, file, rules, defaults=[]) -> None:
    """
    """
    _logger.debug(f'- source: {source}')
    _logger.debug(f'- target: {target}')
    walk(
        source=source,
        target=target,
        file=file,
        rules=_compile_rules(rules=rules),
        defaults=defaults,
    )

def walk(source, target, file, rules, defaults) -> None:
    """
    """
    _logger.debug(f'> {file}')

    ### Process all fules
    for _file_full_path in ( source/file ).glob('*'):
        _file = _file_full_path.relative_to(source)

        ###
        ### Process rules
        ###
        _handled = False
        for rule in _rule_filter_by_filename(filename=str(_file), rules=rules):
            _handled = True
            _rule_safe_call_command(
                source=source,
                target=target,
                file=_file,
                rule=rule
            )
        
        ###
        ### No toute found
        ###
        if not _handled:
            for rule in defaults:
                _rule_safe_call_command(
                    source=source,
                    target=target,
                    file=_file,
                    rule=rule
                )

            if _file_full_path.is_dir():
                (target / _file ).mkdir(mode=0o750, exist_ok=True)
                walk(
                    source=source,
                    target=target,
                    file=_file,
                    rules=rules,
                    defaults=defaults
                )
    
###
### Internal helpers
###

def _compile_rules(rules):
    """
    """
    _logger.debug('- rules:')

    ### Variables
    _rules = []

    ### Loop through all rules
    for rule in rules:
        _logger.debug(f'  pattern: {rule["pattern"]}')

        ### Build pattern
        rule['pattern'] = re.compile(rule['pattern'])

        ### Done
        _rules.append(rule)
    
    ### Completed
    return _rules    


def _rule_filter_by_filename(filename, rules):
    """
    """
    return list( filter( lambda r: r['pattern'].search(filename), rules) )


def _rule_safe_call_command(source, target, file, rule) -> None:
    """
    """
    try:
        _logger.info(f'  {file}')
        call(rule['command'],
            source=source,
            target=target,
            file=file
        )
    
    ### Catch error
    except Exception as ex:
        if _logger.level <= logging.DEBUG:
            _logger.exception(ex)
        else:
            _logger.warning(f'Command "{rule["command"]}" failed with error: {ex}')
