from phasme.config import Config
import shutil

###
### Command entry point
###
def run(*args)->None:
    _target = Config().path('temp')
    shutil.rmtree( _target )
    _target.mkdir(mode=0o750)
