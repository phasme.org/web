from pathlib import Path
from phasme.config import Config
from phasme.utils import call
import logging
import re

### Global variables
_logger = logging.getLogger(__name__)

###
### Command entry point
###
def run(*args)->None:
    """
    """
    _logger.info('Building theme ...')

    ### Variables
    config = Config()
    source = Path( config.get(__name__)['source'] )
    target = Path( config.get(__name__)['target'] )

    ### Loop through rules
    call('build.action.walk',
        source=source,
        target=target,
        file='.',
        rules=config.get(__name__)['rules'],
    )

    ### Completed
    _logger.info('Build theme complete.')
