from pathlib import Path
from phasme.config import Config
from phasme.engine import Engine
import logging
import uuid
import yaml

### Create logger
_logger = logging.getLogger(__name__)

### Command entry point
def run(source, file, target, **kvargs)->None:
    """
    """
    _config = Config()

    ### 
    ### Build default meta data
    ###
    _meta = { 'config': _config.all() }
    _meta['path'] = _meta['config']['path']
    _meta['data'] = _meta['config']['data']
    _meta['env'] = _meta['config']['env']

    ###
    ### Build source file
    ###
    _temp_dir = _config.path('temp')
    _temp_dir.mkdir(mode=0o750, exist_ok=True)

    ### Unpack source file
    _file_meta, _file_tmpl = _unpack_file(file=source / file, temp=_temp_dir)

    ### Load meta
    if _file_meta.exists():
        with open(_file_meta, mode='rt') as _handler:
            _meta.update( yaml.safe_load(_handler) )

    ###
    ### Build target file
    ###
    search = [
        str( _config.path('temp') ),
        str( source ),
        str( _config.path('resources') / 'view' ),
        str( _config.path('theme') / 'view' ),
    ]

    ### Get engine
    _engine = Engine(search_path=search)

    ### Get tempalte
    _template = _engine.get_template(_file_tmpl.name)

    ### Generate target path
    _target = target / file
    if _target.suffix == '.j2':
        _target = _target.with_suffix('')

    ### Completed
    _logger.debug(f'- Generating "{_target}"')
    _template.stream(**_meta).dump(str( _target ))

def _generate_unique_file_name(dir):
    """
    """
    return dir / uuid.uuid4().hex

def _unpack_file(file, temp) -> Path:
    """
    """
    _logger.debug('- split source file')

    ### Variables
    file_temp_name = _generate_unique_file_name(dir=temp)

    ### Split source file into yml & j2
    with open(file, mode='rt', encoding='utf-8') as handler:
        row = handler.readline()

        ###
        ### Extract meta data
        ###
        file_meta = file_temp_name.with_suffix('.yml')
        _logger.debug(f'  meta data: {file_meta}')

        ### Save data
        if row[:3] == '---':
            with open(file_meta, mode='wt', encoding='utf-8') as _io:
                while row and row[:3] != '===':
                    _io.write(row)
                    row = handler.readline()
            row = handler.readline()

        ###
        ### Extract template
        ###
        file_template = file_temp_name.with_suffix('.j2')
        _logger.debug(f'  template: {file_template}')

        ### Save data
        with open(file_template, mode='wt', encoding='utf-8') as _io:
            _io.write(row)
            for row in handler:
                _io.write(row)

    ### Completed
    return file_meta, file_template
