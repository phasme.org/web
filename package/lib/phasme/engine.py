from jinja2 import Environment, FileSystemLoader
from phasme.config import Config
from ext.engine.stack import StackExtension

import logging

class Engine:

    def __init__(self,**kvargs) -> None:
        """
        """
        self.__logger = logging.getLogger(__name__)
        self.__engine = self.__build_engine(**kvargs)

    ###
    ### Engine
    ###

    def __build_engine(self, search_path=None):
        """
        """
        self.__logger.debug("Building template engine")

        ### Variables
        config = Config()

        ### Build environment
        loader = self.__build_engine_loader(config, search_path)
        extensions = self.__build_engine_extensions(config)

        ### Completed
        return Environment(
            autoescape=True,
            loader=loader,
            extensions=extensions
        )

    def __build_engine_loader(self, config, search_path):
        """
        """    
        self.__logger.debug('- building loader')
        self.__logger.debug(f'  search path: {search_path}')

        ### Completed
        return FileSystemLoader(searchpath=search_path)

    def __build_engine_extensions(self, config) -> list:
        """
        """
        self.__logger.debug('- building extensions')

        ### Variables
        extensions = []

        ### Adding stack
        extensions.append( StackExtension )

        ### Completed
        return extensions

    ###
    ### Public interface
    ###

    def get_template(self, name:str):
        """
        """
        return self.__engine.get_template(name)