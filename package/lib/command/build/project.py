from pathlib import Path
from phasme.config import Config
from phasme.utils import call
import logging
import re

### Global variables
_logger = logging.getLogger(__name__)

###
### Command entry point
###
def run(*args)->None:
    """
    Build project.

        This command only handles files in <source> directory, according to configured rules.

    Options:
        None

    Notes:
      <source> is {{ path['source'] }}
    """
    _logger.info('Building project ...')

    ### Variables
    config = Config()
    source = Path( config.get(__name__)['source'] )
    target = Path( config.get(__name__)['target'] )

    ### Loop through rules
    call('build.action.walk',
        source=source,
        target=target,
        file='.',
        rules=config.get(__name__)['rules'],
        defaults=config.get(__name__)['default_rules'],
    )

    ### Completed
    _logger.info('Build project complete.')
